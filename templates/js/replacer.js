$(document).ready(() => {

    switch (window.location.pathname) {
        case '/':
            $('body').attr('id', 'home');
            $("#nav-link-home").addClass("active");
            $(".navbar-light").addClass("navbar-dark");
            $(".navbar-dark").removeClass("navbar-light");
            break;
        case '/about':
            $("#nav-link-about").addClass("active");
            break;
        case '/studies':
            $("#nav-link-studies").addClass("active");
            break;
        case '/contact-us':
            $('body').attr('id', 'contact');
            $("#nav-link-contact-us").addClass("active");
            $(".navbar-light").addClass("navbar-dark");
            $(".navbar-dark").removeClass("navbar-light");
            break;
    }
});

// Navbar

$(window).scroll(function() {

    var scroll = $(window).scrollTop();

    if (scroll > 0) {

        $(".navbar-dark").addClass("navbar-light");
        $(".navbar-dark").addClass("navbar-dark-scrolled");
        $(".navbar-dark-scrolled").removeClass("navbar-dark");

        $(".navbar").addClass("bg-white");

    } else {

        $(".navbar-dark-scrolled").removeClass("navbar-light");
        $(".navbar-dark-scrolled").addClass("navbar-dark");
        $(".navbar-dark").removeClass("navbar-dark-scrolled");

        $(".navbar").removeClass("bg-white");
    }

});

