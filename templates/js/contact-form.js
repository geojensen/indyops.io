const messagePlaceholder = 'Put your message here'
const emailPlaceholder = 'Your Email address'

$('#message').val(messagePlaceholder)

// $('body').on('click', () => {
//     if ($('#from-email').val().trim() === '')
//         $('#from-email').val(emailPlaceholder)
//
//     if ($('#message').val().trim() === '')
//         $('#message').val(messagePlaceholder)
// });

$('#from-email').on('click', () => {
    let value = $('#from-email').val().trim()
    if (value === emailPlaceholder)
        $('#from-email').val('')
});

$('#message').on('click', () => {
    let value = $('#message').val().trim()
    if (value === messagePlaceholder)
        $('#message').val('')
});
